/************************************************************************
 * Project           		: shakti devt board
 * Name of the file		: uart.h
 * Brief Description of file    : Header file for uart.
 * Name of Author    	        : Niketan Shahapur,Kotteeswaran & Sathya Narayanan N
 * Email ID                     : niketanshahpur@gmail.com, kottee.1@gmail.com, sathya281@gmail.com

  Copyright (C) 2019  IIT Madras. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
************************************************************************/
/**
 * @file uart.h
 * @project shakti devt board
 * @brief Header file for uart
 */

#ifndef UART_H
#define UART_H
#include <stdint.h>

#define STS_RX_NOT_EMPTY    1 << 2
#define STS_TX_FULL         1 << 1

/* Struct to access UART registers as 32 bit registers */
typedef struct
{
	unsigned short baud;	 /*! Baud rate configuration Register -- 16 bits*/
	unsigned short reserv0;	 /*! reserved */
	unsigned int  tx_reg;	 /*! Transmit register -- the value that needs to be tranmitted needs to be written here-32 bits*/
	unsigned int  rcv_reg;	 /*! Receive register -- the value that received from uart can be read from here --32 bits*/
	unsigned char  status;	 /*! Status register -- Reads various transmit and receive status - 8 bits*/
	unsigned char  reserv1;	 /*! reserved */
	unsigned short  reserv2; /*! reserved */
	unsigned short delay;    /*! Delays the transmit with specified clock - 16bits*/
	unsigned short reserv3;  /*! reserved */
	unsigned short control;   /*! Control Register -- Configures the no. of bits used, stop bits, parity enabled or not - 16bits*/
	unsigned short reserv5;  /*! reserved */
	unsigned char ien;	     /*! Enables the required interrupts - 8 bits*/
	unsigned char reserv6;   /*! reserved */
	unsigned short reserv7;  /*! reserved */
	unsigned char  iqcycles; /*! 8-bit register that indicates number of input qualification cycles - 8 bits*/
	unsigned char reserv8;   /*! reserved */
	unsigned short reserv9;  /*! reserved */
#ifdef USE_RX_THRESHOLD /*! This is to be used only when support is there. */
	unsigned char rx_threshold;	/*! RX FIFO size configuration register - 8 bits*/
	unsigned char reserv10;    /*! reserved */ 
	unsigned short reserv11;    /*! reserved */
#endif	
} uart_struct;

extern uart_struct *uart_instance[0];

void uart_init();

#undef putchar
int putchar(int ch);

#undef getchar
int getchar();

#endif
