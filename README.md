# spiking

A software development environment to write programs and simulate in spike

##Prerequisite

For Ubuntu 16.04/Ubuntu 18.04, please use ready to use [shakti-tools](https://gitlab.com/shaktiproject/software/shakti-tools.git).

For others, Use [riscv-tools](https://gitlab.com/shaktiproject/software/riscv-tools) for all platforms.


##Download

 git clone https://gitlab.com/shaktiproject/software/spiking.git 
 cd spiking

##Compilation commands

####Assembly file (.S extension)

	riscv64-unknown-elf-gcc -nostdlib -nostartfiles -T spike.lds example1.S -o example1.elf

####C file (.c extension)

	riscv64-unknown-elf-gcc -g hello.c -o hello.elf 

####Generating Disassembled file (Object dump)

	riscv64-unknown-elf-objdump -D hello.elf

####Generating Intermediate assembly file from .c file

	riscv64-unknown-elf-gcc -S sum.c  
